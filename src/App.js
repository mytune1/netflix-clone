import React from 'react';
import './App.css';
import Row from './Row';
import Request from './Request';
import Banner from "./Banner";
import Nav from "./Nav";

function App() {
  return (
    <div className="App">
      <Nav />
      <Banner />
      <Row title="Netflix Originals" fetchUrl={Request.fetchNetflixOriginals} isLargeRow={true} />
      <Row title="Trainding Now" fetchUrl={Request.fetchTranding} />
      <Row title="Top Rated" fetchUrl={Request.fetchTopRated} />
      <Row title="Action Movies" fetchUrl={Request.fetchActionMovies} />
      <Row title="Comedy Movies" fetchUrl={Request.fetchComedyMovies} />
      <Row title="Horror Movies" fetchUrl={Request.fetchHorrorMovies} />
      <Row title="Romantic Movie" fetchUrl={Request.fetchRomanceMovies} />
      <Row title="Documentries" fetchUrl={Request.fetchDocumentries} />
    </div>
  );
}

export default App;
