import React, { useState, useEffect } from 'react';
import axiosUrl from './axiosUrl';
import YouTube from 'react-youtube';
import movieTrailer from 'movie-trailer';

const baseUrl = "https://image.tmdb.org/t/p/original/"
function Row({ title, fetchUrl, isLargeRow }) {
	const [movies, setMovies] = useState([]);
	const [trailerUrl, setTrailerUrl] = useState("");

	useEffect(() => {
		async function fetchData() {
			const request = await axiosUrl.get(fetchUrl);
			setMovies(request.data.results)
			return request;
		}
		fetchData();
	}, [fetchUrl]);

	const opts = {
		height: '390',
		width: '640',
		playerVars: {
			autoplay: 1,
			// origin: 'http://localhost:3000'
		},
	};
	const handelClick = (movies) => {
		if (trailerUrl) {
			setTrailerUrl('')
		}
		else {
			movieTrailer(movies?.name || "")
				.then(url => {
					const urlParams = new URLSearchParams(new URL(url).search);
					setTrailerUrl(urlParams.get('v'));
				}).catch(error => console.log(error));
		}
	}

	return (
		<div className="app__area">
			<h2>{title}</h2>
			<div className={`movie_card_list ${isLargeRow && "movie_card_list_Large"}`}>
				{movies.map((item) => {
					return <img key={item.id} onClick={() => handelClick(item)} src={`${baseUrl}${isLargeRow ? item.poster_path : item.backdrop_path}`} alt={item.title} />
				})}
			</div>
			{trailerUrl && <YouTube videoId={trailerUrl} opts={opts} />}
		</div>
	)
}

export default Row;