import react, { useState, useEffect } from 'react';
import './Nav.css';

function Nav() {
    const [show, handelShow] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > 100) {
                handelShow(true);
            } else handelShow(false);
        });
        return () => {
            window.removeEventListener("scroll");
        }
    }, []);

    return (
        <div className={`nav ${show && "nav_black"}`}>
            {/* <div> */}
            <img src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png" style={{ width: "100px" }} />
        </div>
    )
}

export default Nav;