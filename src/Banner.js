import React, { useEffect, useState } from 'react';

import axios from './axiosUrl';
import Request from './Request';

import "./banner.css";


function Banner() {
    const [movie, setMovies] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const request = await axios.get(Request.fetchNetflixOriginals);
            setMovies(request.data.results[
                Math.floor(Math.random() * (request.data.results.length - 1))
            ])
            return request;
        }
        fetchData()
    }, []);
    console.log(movie);
    return (
        <header className="banner"
            style={{
                backgroundImage: `url("https://image.tmdb.org/t/p/original/${movie?.backdrop_path}")`
            }}
        >
            <div style={{ width: '100%' }}>
                <div className="banner__content">
                    <h3>{movie?.title || movie?.name || movie?.original_name}</h3>
                    <div className="banner_buttons">
                        <button className="banner_button">Play</button>
                        <button className="banner_button">Watch List</button>
                    </div>
                    <p className="banner_discription">{movie?.overview}</p>
                </div>
                <div className="banner_fadedbottom"></div>
            </div>
        </header>
    )
}

export default Banner;